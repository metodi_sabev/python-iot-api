from socket import *


class EthernetConnection:

    def __init__(self, ip, port = 5000):
        self.address = (ip, port)
        self.client_socket = socket(AF_INET, SOCK_DGRAM)
        self.client_socket.settimeout(1)

    def send(self, message):
        self.client_socket.sendto(message, self.address)

    def read(self):
        data, address = self.client_socket.recvfrom(2048)

        return data
