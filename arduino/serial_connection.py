import os, pty, serial

class SerialConnection:

    def __init__(self):
        self.master, self.slave = pty.openpty()
        self.s_name = os.ttyname(self.slave)

        self.serial_connection = serial.Serial(self.s_name)

    def write(self):
        self.serial_connection.write('r:201\n')
        self.serial_connection.write('g:15\n')

    def read(self):
        print os.read(self.master, 1000)
