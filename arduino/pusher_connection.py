from django.conf import settings
import pusher


class PusherConnection:

    def __init__(self):
        self.pusher_client = pusher.Pusher(
            app_id=settings.PUSHER_APP_ID,
            key=settings.PUSHER_KEY,
            secret=settings.PUSHER_SECRET,
            cluster=settings.PUSHER_CLUSTER,
            ssl=True
        )
        self.channel = settings.PUSHER_CHANNEL
        self.event = settings.PUSHER_EVENT

    def send(self, message):
        return self.pusher_client.trigger(self.channel, self.event, message)
