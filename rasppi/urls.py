"""rasppi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, patterns
from django.contrib import admin
from rest_framework import routers

from api.views import ApiView

router = routers.SimpleRouter()
router.register(r'invoke', ApiView, base_name='get')

urlpatterns = patterns(
    '',
    url(r'^api/v1/', include(patterns(
        '',
        url(r'^invoke/$', ApiView.as_view(), name="invoke_server"),
        url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
        # url(r'^', include(router.urls, namespace='api/v1')),
    )))
)

# urlpatterns = [
#     url(r'^api/v1/', include('api.urls')),
#     # url(r'^admin/', admin.site.urls),
# ]
