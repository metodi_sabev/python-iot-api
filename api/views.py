from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import View

from arduino.serial_connection import SerialConnection
from arduino.pusher_connection import PusherConnection


class ApiView(View):

    def get(self, request, *args, **kwargs):
        pusher = PusherConnection()
        pusher.send({"message" : "Alalalala"})

        json_response = {'Success': True}
        # self.pusher_client.trigger('metodi-arduino', 'my_event', {'message': 'hello world'})
        json_response['message'] = 'Test'
        return JsonResponse(json_response)
